import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

// Image class
public class Image {
	private String NAME;
	private String PATH;
	private float[] HISTOGRAM = new float[600]; // We picked 600 because we split H to 6, S to 10 and V to 10
	
	public Image(String name, String path){ // Object constructor
		PATH = path;
		NAME = name;
	}
	
	public void setHist(float[] ch){ // Set histogram value
		HISTOGRAM = ch;
	}
	
	public void createHist() throws IOException{ // Create a new histogram
		int[][][] ch = new int[6][10][10];
		BufferedImage image = ImageIO.read(new File(PATH)); // Read an image
		for(int w = 0; w < image.getWidth(); w++){
			for(int h = 0; h < image.getHeight(); h++){ // Get every pixel RGB value
				int color = image.getRGB(w, h);
				int red = (color & 0x00ff0000) >> 16;
				int green = (color & 0x0000ff00) >> 8;
				int blue = color & 0x000000ff;
				float[] hsv = new float[3];
				Color.RGBtoHSB(red, green, blue, hsv); // Convert RGB to HSV/HSB
				int hue = (int)(hsv[0] * 360);
				int sat = (int)(hsv[1] * 100);
				int val = (int)(hsv[2] * 100);
				if(hue == 360){ // Set hue 360 to index 5
					hue -= 1;
				}
				if(sat == 100){ // Set sat 100 to index 9
					sat -= 1;
				}
				if(val == 100){ // Set val 100 to index 9
					val -= 1;
				}
				ch[hue/60][sat/10][val/10]++; // Each HSV value are store in 6 10 10 array
			}
		}
		int cnt = 0;
		float pixels = image.getWidth() * image.getHeight();
		for(int i = 0; i < ch.length; i++){
			for(int j = 0; j < ch[i].length; j++){
				for(int k = 0; k < ch[i][j].length; k++){
					HISTOGRAM[cnt] = ch[i][j][k]/pixels; // Normalize number counted in index by divided by number of image pixels
					cnt++;
				}
			}
		}
	}
	
	public String getName(){
		return NAME;
	}
	
	public String getPath(){
		return PATH;
	}
	
	public float[] getHist(){
		return HISTOGRAM;
	}
	
	@Override
	public String toString(){
		return NAME + " - " + PATH + " - " +  HISTOGRAM; //For JSON
	}
	
	public float compare(Image b){ // Cosine similarity
		float similarity = 0;
		float dotProduct = 0;
		float magnitudeA = 0;
		float magnitudeB = 0;
		for(int i = 0; i < 600; i++){ 
			dotProduct = dotProduct + HISTOGRAM[i]*b.getHist()[i];
			magnitudeA = magnitudeA + HISTOGRAM[i]*HISTOGRAM[i];
			magnitudeB = magnitudeB + b.getHist()[i]*b.getHist()[i];
		}
		magnitudeA = (float) Math.sqrt(magnitudeA);
		magnitudeB = (float) Math.sqrt(magnitudeB);
		similarity = (dotProduct)/(magnitudeA*magnitudeB);
		return similarity;
	}

}
