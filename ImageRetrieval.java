import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;


public class ImageRetrieval {
	public static void main(String[] args) throws Exception{
		
		// Get paths
		String imagePath = args[0];
		String indexPath = args[1];
		String queryImage = args[2];
//		String imagePath = "D:/workspace/cats/base";
//		String indexPath = "D:/workspace/cats/index";
//		String queryImage = "D:/workspace/cats/test.jpg";
		
		// Load images
		List<Image> imageList = listImage(imagePath, indexPath);
		
		// Load test image
		String[] spl = queryImage.split("/");
		String name = spl[spl.length-1];
		Image test = new Image(name, queryImage);
		test.createHist(); // Create HSV histogram
		
		float[] simi = new float[imageList.size()]; // Store cosine similarity
		int cnt = 0;
		
		// Print out results
		System.out.println("Higher number means more similar.");
		for(Image image : imageList){
			simi[cnt] = test.compare(image);
			System.out.println("Similarity to " + image.getPath() + " by " + simi[cnt]); // Print path and similarity value
			cnt++;
		}
	}
		
	// Find images path
	public static List<Image> listImage(String imgPath, String inPath) throws IOException{
		List<Image> result = new ArrayList<Image>();
		File directory = new File(imgPath);
		File[] fileList = directory.listFiles();
		for (File file : fileList){
			if (file.isFile()){
				result.add(loadImage(file, inPath)); // Image found; load it
		    }
		}
		
		return result;
	}
	
	// Load an image
	public static Image loadImage(File file, String inPath) throws IOException{
		try{
			String str = inPath + "/" + file.getName().replace(".jpg", ".json"); // Try to find already extracted images in JSON
			InputStream is = new FileInputStream(str);
			InputStreamReader isr = new InputStreamReader(is);
			
			String name = "";
			String path = "";
			float[] hist = new float[600];
			
			JsonReader reader = new JsonReader(isr); // Begin reading data in JSON
			while(reader.hasNext()){
				JsonToken token = reader.peek();
				String key = "";
				switch(token){
				case BEGIN_OBJECT:
					reader.beginObject();
					break;
				case NAME:
					key = reader.nextName();
					if(key.equals("NAME")){ // Name found!
						name = reader.nextString();
					}else if(key.equals("PATH")){ // Path found !
						path = reader.nextString();
					}
					break;
				case BEGIN_ARRAY:
					reader.beginArray(); // Histogram array found !
					for(int i = 0; i < 600; i++){
						float num = (float)reader.nextDouble();
						hist[i] = num;
					}
					reader.endArray();
					break;
				case BOOLEAN:
					break;
				case END_ARRAY:
					break;
				case END_DOCUMENT:
					break;
				case END_OBJECT:
					break;
				case NULL:
					break;
				case NUMBER:
					break;
				case STRING:
					break;
				default:
					break;
				}
			}
			reader.close();
			Image im = new Image(name, path); // Create an object by JSON file
			im.setHist(hist); // Set histogram
			return im;
		}catch(IOException e){ // In case we can't find json file
			Image im = new Image(file.getName(), file.getAbsolutePath()); // Create an object
			im.createHist(); // Create histogram
			String str = inPath + "/" + file.getName().replace(".jpg", ".json"); 
			try(Writer writer = new OutputStreamWriter(new FileOutputStream(str) , "UTF-8")){ // Create a json file for the object
	            Gson gson = new GsonBuilder().create();
	            gson.toJson(im, writer);
	        }
			return im;
		}
	}
}
